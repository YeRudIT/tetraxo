#!/bin/python3

from enum import Enum

import board_to_png as b2p


class Player(Enum):
    d = 0
    x = 1
    o = 5


X: Player = Player.x
O: Player = Player.o
D: Player = Player.d


class MiniBoard:
    won: Player | None = None
    board: list[Player | None]

    def __init__(self) -> None:
        self.board = [None for _ in range(0, 9)]


class BigBoard:
    active: int | None
    boards: list[MiniBoard]

    def __init__(self, active = None) -> None:
        self.active = active
        self.boards = [MiniBoard() for _ in range(0, 9)]


def get_mb(bb: BigBoard, index: int | None) -> MiniBoard | None:
    if index is None:
        return None

    if 0 <= index < 9:
        return bb.boards[index]
    return None


def mb_to_str(mb: MiniBoard) -> str:
    return f"""
    {mb.won=}
    {mb.board}
    """


def get_active_mb(
    bb: BigBoard,
):
    return get_mb(bb, bb.active)


def bb_to_str(bb: BigBoard) -> str:
    boards: str = "\n".join([mb_to_str(boar) for boar in bb.boards])
    return f"""
        {bb.active=}
        {get_active_mb(bb)=}
        {boards}
        """


def bb_to_svg(bb: BigBoard, current_p: Player):
    board: list[str] = []
    for in_mb, mb in enumerate(bb.boards):
        for in_cell, cell in enumerate(mb.board):
            if cell is None:
                continue
            func = b2p.o if cell == O else b2p.x
            board.append(func(in_mb, in_cell, False))
        if mb.won is not None and mb.won != D:
            func = b2p.o if mb.won == O else b2p.x
            board.append(func(in_mb))

    if bb.active is not None:
        board.append(b2p.rect(bb.active, current_p == O))

    b2p.board_to_png(board)


def mini_won(mb: MiniBoard, current: Player) -> bool:
    def consecutives(
        board: list[Player | None], start: int, end: int, step: int, plw
    ) -> bool:
        sum: int = 0
        for i in range(start, end, step):
            cell = board[i]
            cell_val = 0
            if cell is not None:
                cell_val = cell.value
            sum = sum + cell_val

        return plw == sum

    plw = current.value * 3

    cons = lambda x, y, z: consecutives(mb.board, x, y, z, plw)

    d1 = cons(0, 9, 4)
    d2 = cons(2, 7, 2)
    c1 = cons(0, 7, 3)
    c2 = cons(1, 8, 3)
    c3 = cons(2, 9, 3)
    r1 = cons(0, 3, 1)
    r2 = cons(3, 6, 1)
    r3 = cons(6, 9, 1)

    return d1 or d2 or c1 or c2 or c3 or r1 or r2 or r3

def mini_draw(mb: MiniBoard) -> bool:
    for cell in mb.board:
        if cell is None: return False
    
    return True


def main():
    # let hod be turn
    proomt = "{player}> "
    current_player: Player = X

    print("Choose one of the big tiles [column and then row; Ex.: 0..8]")

    big_b = BigBoard(None)
    mini_bord_w = MiniBoard()

    bb_to_svg(big_b, current_player)

    big_hod = input(proomt.format(player=current_player))
    big_b.active = int(big_hod)

    bb_to_svg(big_b, current_player)

    print("Choose one of the tiles [column and then row; Ex.: 0..8]")

    while True:
        hod = input(proomt.format(player=current_player))
        active_mb = get_active_mb(big_b)
        if active_mb is None:
            raise ValueError("why is active_mini_board None?")

        if active_mb.board[int(hod)] is not None:
            print("celula ocupata")
            continue
        active_mb.board[int(hod)] = current_player
            

        if mini_won(active_mb, current_player):
            mini_bord_w.board[big_b.active] = current_player
            active_mb.won = current_player

        elif mini_draw(active_mb):
            active_mb.won = D

        if mini_won(mini_bord_w, current_player):
            bb_to_svg(big_b, current_player)
            print(f"{current_player} won!!!")
            break
        elif mini_draw(mini_bord_w):
            bb_to_svg(big_b, current_player)
            print(f"Both lost, lol!")
            break

        current_player = O if current_player == X else X

        big_b.active = None
        bb_to_svg(big_b, current_player)
        while big_b.boards[int(hod)].won is not None:
            print("Choose one of the big tiles [column and then row; Ex.: 0..8]")
            hod = input(proomt.format(player=current_player))

        big_b.active = int(hod)

        bb_to_svg(big_b, current_player)


if __name__ == "__main__":
    main()
