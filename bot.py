#!/usr/bin/env python3

import os
from pathlib import Path

from telebot import TeleBot, types as T
from telebot.util import quick_markup

import tetraxo as txo

HNELP_EN = """
A bot to play <a href="https://codeberg.org/YeRudIT/tetraxo">tetraxo</a>.

/hnelp - hnelps
/joaca_bai - joaca hei hei
"""


HNELP = """
Un robot pentru a juca <a href="https://codeberg.org/YeRudIT/tetraxo">tetraxo</a>.

/hnelp - anjută
/joaca_bai - joaca hei hei
"""

MD = "MarkdownV2"
HTML = "html"

class Jucaus:
    id: int
    simbol: txo.Player

    def __init__(self, id_jucator: int, sim_jucator: txo.Player) -> None:
        self.id = id_jucator
        self.simbol = sim_jucator

class Joc:
    tabla: txo.BigBoard
    table_vici: txo.MiniBoard

    jucaus_x: Jucaus
    jucaus_o: Jucaus

    jucausul: Jucaus
    
    def __init__(self, id_x: int) -> None:
        self.tabla = txo.BigBoard()
        self.table_vici = txo.MiniBoard()
        self.jucaus_x = Jucaus(id_x, txo.X)
        self.jucausul = self.jucaus_x


jocuri = {}

def main():
    TOKEN = os.environ["KEY_TOKEN"]
    bot = TeleBot(TOKEN)

    bot.register_message_handler(start, pass_bot=True, commands=["help", "start", "hnelp"])
    bot.register_message_handler(play, pass_bot=True, commands=["joaca_bai", "joaca"])

    bot.register_callback_query_handler(adauga_jucator, pass_bot=True, func = lambda call: len(call.data.split(':')) == 2)
    bot.register_callback_query_handler(game, pass_bot=True, func = lambda call: len(call.data.split('-')) == 2)



    bot.infinity_polling(allowed_updates=["message", "callback_query"])

def start(msg: T.Message, bot: TeleBot):
    bot.send_message(msg.chat.id, HNELP, parse_mode=HTML)

def play(msg: T.Message, bot: TeleBot):
    nou_joc = Joc(msg.from_user.id)

    txo.bb_to_svg(nou_joc.tabla, nou_joc.jucausul.simbol)
    photo_m = bot.send_photo(msg.chat.id, caption="Jocul îi creat. Mai așteptăm un jucator...",  photo=T.InputFile(Path().cwd() / "board.png"))
    id_joc = f"{photo_m.id}:{msg.chat.id}"
    jocuri[id_joc] = nou_joc

    print(jocuri)

    tasta = quick_markup({"Joacă": {"callback_data": id_joc}})

    bot.edit_message_reply_markup(photo_m.chat.id, photo_m.id, reply_markup=tasta)


def adauga_jucator(call: T.CallbackQuery, bot: TeleBot):
    id_joc = call.data
    id_jucaus = call.from_user.id

    joc: Joc = jocuri.get(id_joc, None)
    print(jocuri)

    if joc is None:
        bot.reply_to(call.message, "jocul nu-i in baza de date")
        bot.edit_message_reply_markup(call.message.chat.id, call.message.id, reply_markup=None)
        return

    if joc.jucaus_x.id == id_jucaus:
        bot.answer_callback_query(call.id, "Gata joci")
        return
    
    joc.jucaus_o = Jucaus(id_jucaus, txo.O)
    taste = T.InlineKeyboardMarkup(board_to_keys(joc.table_vici, False))

    bot.edit_message_caption(chat_id=call.message.chat.id, caption="Alege mini-tabla:", message_id=call.message.id, reply_markup=taste)

def board_to_keys(tabla: txo.MiniBoard, tabla_mic = True):
    keyboard = []

    row = []

    car = " "

    for i, cell in enumerate(tabla.board):
        match cell:
            case None:
                car = " "
            case txo.X:
                car = "X"
            case txo.O:
                car = "O"
            case _:
                car = "?"
        row.append(T.InlineKeyboardButton(car, callback_data=f"{'xo' if tabla_mic else 'XO'}-{i}"))
        if len(row) == 3:
            keyboard.append(row)
            row = []

    return keyboard

def game(call: T.CallbackQuery, bot: TeleBot):
    print("game: ",jocuri)
    print(f"gaym_id: {call.message.id}:{call.message.chat.id}")

    joc: Joc = jocuri.get(f"{call.message.id}:{call.message.chat.id}", None)
    c_button: list = call.data.split('-')

    if joc is None:
        bot.reply_to(call.message, "jocul nu-i in baza de date")
        bot.edit_message_reply_markup(call.message.chat.id, call.message.id, reply_markup=None)
        return
    
    if call.from_user.id != joc.jucausul.id:
        print(joc.jucausul)
        bot.answer_callback_query(call.id, f"La moment merge jucatorul {joc.jucausul.id}")
        return

    if c_button[0] == "XO":
        min_board: txo.MiniBoard = joc.tabla.boards[int(c_button[1])]
        if min_board.won is not None:
            taste = T.InlineKeyboardMarkup(board_to_keys(joc.table_vici, False))

            bot.edit_message_caption(chat_id=call.message.chat.id, caption="Alege mini-tabla:", message_id=call.message.id, reply_markup=taste)
            return
        joc.tabla.active = int(c_button[1])

        taste = T.InlineKeyboardMarkup(board_to_keys(min_board))
        txo.bb_to_svg(joc.tabla, joc.jucausul.simbol)
        bot.edit_message_media(
            media=T.InputMediaPhoto(open("board.png", "rb")),
            chat_id=call.message.chat.id,
            message_id=call.message.id,
            reply_markup=taste
        )
        return


    if c_button[0] == "xo":
        active_id = joc.tabla.active

        if active_id is None:
            raise ValueError("Why none?")


        min_board: txo.MiniBoard = joc.tabla.boards[active_id]

        if min_board.board[int(c_button[1])] is not None:
            bot.answer_callback_query(call.id, f"Celula ocupata")
            return


        min_board.board[int(c_button[1])] = joc.jucausul.simbol

        if txo.mini_won(min_board, joc.jucausul.simbol):
            min_board.won = joc.jucausul.simbol
            joc.table_vici.board[active_id] = joc.jucausul.simbol

        elif txo.mini_draw(min_board):
            min_board.won = txo.D

        if txo.mini_won(joc.table_vici, joc.jucausul.simbol): 
            joc.table_vici.won = joc.jucausul.simbol
            txo.bb_to_svg(joc.tabla, joc.jucausul.simbol)
            bot.edit_message_media(
                media=T.InputMediaPhoto(open("board.png", "rb")),
                chat_id=call.message.chat.id,
                message_id=call.message.id,
                reply_markup=None
            )
            bot.edit_message_caption(
                chat_id=call.message.chat.id,
                caption=f"{call.from_user.full_name} a cistigat",
                message_id=call.message.id,
            )
            return
            

        elif txo.mini_draw(joc.table_vici):
            joc.table_vici.won = txo.D
            txo.bb_to_svg(joc.tabla, joc.jucausul.simbol)
            bot.edit_message_media(
                media=T.InputMediaPhoto(open("board.png", "rb")),
                chat_id=call.message.chat.id,
                message_id=call.message.id,
                reply_markup=None
            )
            bot.edit_message_caption(
                chat_id=call.message.chat.id,
                caption=f"ambii ati pierdut",
                message_id=call.message.id,
            )
            return

        if joc.tabla.boards[int(c_button[1])].won is not None:
            taste = T.InlineKeyboardMarkup(board_to_keys(joc.table_vici, False))
            joc.jucausul = joc.jucaus_x if joc.jucausul == joc.jucaus_o else joc.jucaus_o
            # FIXME: Ar trebui sa nu fie evidentiata nicio tabla
            joc.tabla.active = None
            txo.bb_to_svg(joc.tabla, joc.jucausul.simbol)
            bot.edit_message_media(
                media=T.InputMediaPhoto(open("board.png", "rb")),
                chat_id=call.message.chat.id,
                message_id=call.message.id,
                reply_markup=taste
            )
            bot.edit_message_caption(
                chat_id=call.message.chat.id,
                caption="Alege mini-tabla:",
                message_id=call.message.id,
                reply_markup=taste
            )
            return


        joc.tabla.active = int(c_button[1])
        joc.jucausul = joc.jucaus_x if joc.jucausul == joc.jucaus_o else joc.jucaus_o

        
        # TODO: Add to function
        taste = T.InlineKeyboardMarkup(board_to_keys(joc.tabla.boards[int(c_button[1])]))
        txo.bb_to_svg(joc.tabla, joc.jucausul.simbol)
        # TODO: Edit text
        bot.edit_message_media(
            media=T.InputMediaPhoto(open("board.png", "rb")),
            chat_id=call.message.chat.id,
            message_id=call.message.id,
            reply_markup=taste
        )
        return


    



if __name__ == "__main__":
    main()
