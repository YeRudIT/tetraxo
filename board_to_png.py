from cairosvg import svg2png

def o( board: int = 0, cell = 0, big = True):
    board_x = board % 3 * 25
    board_y = board // 3 * 25
    cell_x = cell % 3 * 25
    cell_y = cell // 3 * 25

    return f"""
    <circle
       style="fill:none;stroke:#935505;stroke-width:1.96;stroke-dasharray:none;stroke-opacity:1"
       id="{board}x{cell}"
       cx="12.5"
       cy="12.5"
       r="9"
       transform="{"" if big else "translate(2.3 2.3)"} translate({board_x},{board_y}) { "" if big else f"scale(0.28) translate({cell_x},{cell_y})"}" />
"""

def rect( board: int = 0, is_o = False):
    board_x = board % 3 * 25.5
    board_y = board // 3 * 25.5

    return f"""
<g
     id="layer1"
     transform="translate({board_x}, {board_y})">
    <rect
       style="fill:none;stroke:{"#935505" if is_o else "#5a00c9" };stroke-width:1;stroke-dasharray:none;stroke-opacity:1"
       id="rect1"
       width="22"
       height="22"
       x="1.5"
       y="1.5" />
  </g>
"""

def x( board: int = 0, cell = 0, big = True):
    board_x = board % 3 * 25
    board_y = board // 3 * 25
    cell_x = cell % 3 * 25
    cell_y = cell // 3 * 25

    return f"""
<g
       id="g7"
       transform="{"" if big else "translate(2.3 2.3)"} translate({board_x},{board_y}) { "" if big else f"scale(0.28) translate({cell_x},{cell_y})"}"
       inkscape:export-filename="tetrax.svg"
       inkscape:export-xdpi="96"
       inkscape:export-ydpi="96">
      <path
         style="fill:none;fill-opacity:1;stroke:#5a00c9;stroke-width:2.246;stroke-dasharray:none;stroke-opacity:1"
         d="M 3.794081,3.7940809 21.125089,21.12509"
         id="path6" />
      <path
         style="fill:none;fill-opacity:1;stroke:#5a00c9;stroke-width:2.246;stroke-dasharray:none;stroke-opacity:1"
         d="M 21.125089,3.7940809 3.794081,21.12509"
         id="path6-9" />
    </g>
"""



svg = """
<svg
   width="75mm"
   height="75mm"
   viewBox="0 0 75 75"
   version="1.1"
   id="svg1"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#eeeeee"
     borderopacity="1"
     inkscape:showpageshadow="0"
     inkscape:pageopacity="0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#201c20"
     inkscape:document-units="mm" />
  <defs
     id="defs1" />
  <g
     inkscape:groupmode="layer"
     id="layer2"
     inkscape:label="board"
     transform="translate(0,0)">
    <path
       style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.06616;stroke-dasharray:none;stroke-opacity:1"
       d="M 25.5,3 V 72"
       id="path3" />
    <path
       style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.06616;stroke-dasharray:none;stroke-opacity:1"
       d="M 50.5,3 V 72"
       id="path3-6" />
    <path
       style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.06616;stroke-dasharray:none;stroke-opacity:1"
       d="M 3,25.53308 H 72"
       id="path3-61" />
    <path
       style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.06616;stroke-dasharray:none;stroke-opacity:1"
       d="M 3,50.53308 H 72"
       id="path3-61-2" />
    <g
       id="g6">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2" />
    </g>
    <g
       id="g6-7"
       transform="translate(25)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-3" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-6" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-1" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-2" />
    </g>
    <g
       id="g6-9"
       transform="translate(50)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-4" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-7" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-8" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-4" />
    </g>
    <g
       id="g6-6"
       transform="translate(0,25)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-1" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-0" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-6" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-3" />
    </g>
    <g
       id="g6-1"
       transform="translate(25,25)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-5" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-5" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-4" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-7" />
    </g>
    <g
       id="g6-93"
       transform="translate(50,25)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-7" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-4" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-5" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-25" />
    </g>
    <g
       id="g6-4"
       transform="translate(0,50)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-30" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-78" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-68" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-8" />
    </g>
    <g
       id="g6-49"
       transform="translate(25,50)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-2" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-06" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-89" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-26" />
    </g>
    <g
       id="g6-5"
       transform="translate(50,50)">
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 9.1956518,3 V 22"
         id="path3-7-0" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 16.07971,3 V 22"
         id="path3-6-5-48" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,9.204761 H 22"
         id="path3-61-9-7" />
      <path
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.293581;stroke-dasharray:none;stroke-opacity:1"
         d="M 3,16.088819 H 22"
         id="path3-61-2-2-1" />
    </g>
  </g>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0,0)">
    {}
  </g>
</svg>
"""

def board_to_png(board: list, board_png_path: str = 'board.png'):

    svg2png(bytestring=svg.format("".join(board)), write_to=board_png_path)

board_to_png([rect(4)])

