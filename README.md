![tetraxo](img/xo.png) 

Tetraxo is a multidimensional version of Ticktacktoe.

# How to play?

The game is based on the rule described in [Vsauce's video](https://yewtu.be/watch?v=_Na3a1ZrX7c) 

The program will output the game into a png with the help of `cairosvg`. You can open it with you favorite image viewer.

# Dependencies

+ `cairosvg` -- It makes generating the game possible. 
+ `rlwrap`{optional} -- Makes the proompt more interactive
+ `nsxiv`{optional} -- Great image viewer. It will automatically load the modified image.
+ `asciiquarium`{from image} -- [Tetra Fishes](https://en.wikipedia.org/wiki/Tetra?lang=en) and wasnot
+ `cmatrix`{from image} -- raise the experience to the power of 4


# Licence

This project is licenced under the Fren's Licence. Any Fren is thus granted the right to use my work for the betterment of one's life and one's self, to modify it according to one's needs and redistribute it under the same Frenly licence. Beware that this project is maintained by me in my free time for my personal needs, so I cannot give any warranty.

